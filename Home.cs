﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace CoGG
{
    public partial class Form1 : Form
    {
        private const int WM_NCHITTEST = 0x84;
        private const int HTCLIENT = 0x1;
        private const int HTCAPTION = 0x2;

        ///
        /// Handling the window messages
        ///
        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);

            if (message.Msg == WM_NCHITTEST && (int)message.Result == HTCLIENT)
                message.Result = (IntPtr)HTCAPTION;
        }
        [STAThread] static void Main(string[] args) { Application.Run(new Form1()); }
        
        public Form1()
        {
            InitializeComponent();
        }

    protected override void OnLoad(EventArgs e)
        {
            PlaceMiddleTop();
            base.OnLoad(e);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ControlBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.Text = String.Empty;
        }

            private void PlaceMiddleTop()
        {
            //Determine "rightmost" screen
            Screen rightmost = Screen.AllScreens[0];
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                    rightmost = screen;
            }

            this.Left = (rightmost.WorkingArea.Right / 2) - (this.Width / 2);
            this.Top = 0;
        }

        private string GetLine(string name, int line)
        {
            using (var sr = new StreamReader(name))
            {
                for (int i = 11; i < line; i++)
                    sr.ReadLine();
                return sr.ReadLine();
            }
        }
        private void Check_Click(object sender, EventArgs e)
        {
            PostLabel.Font = new Font("Arial", 18, FontStyle.Bold);
            PostLabel.ForeColor = Color.Orange;
            PostLabel.Text = "Loading";
            listView1.Items.Clear();



            int i = int.Parse("0");
            // Put Pilots to string
            string name = Clipboard.GetText(TextDataFormat.Text);
            using (StringReader sr = new StringReader(name))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    // Webclient to download data from CVA KoS for pilots
                    WebClient myWebClient = new WebClient();
                    byte[] myDataBuffer = myWebClient.DownloadData("http://kos.cva-eve.org/api/?c=json&type=pilot&q=" + line);
                    string download = Encoding.ASCII.GetString(myDataBuffer);
                    // Debugging Outputs
                    // Console.WriteLine(download);
                    string kos = "\"kos\": true,";
                    string results = "\"total\": 0,";
                    string greyresults = "\"npc\": true,";
                    if (download.Contains(kos))
                    {
                        i = i + 1;
                        ListViewItem red = new ListViewItem()
                        {
                            ForeColor = Color.Red,
                            Text = line
                        };
                        listView1.Items.Add(red);

                    }
                    else if (download.Contains(results))
                    {
                        PostLabel.Font = new Font("Arial", 18, FontStyle.Bold);
                        PostLabel.ForeColor = Color.Orange;
                        PostLabel.Text = "Error Not All Players Detected";
                        ListViewItem orange = new ListViewItem()
                        {
                            ForeColor = Color.Orange,
                            Text = line
                        };
                        listView1.Items.Add(orange);
                    }
                    else if (download.Contains(greyresults))
                    {
                        PostLabel.Font = new Font("Arial", 18, FontStyle.Bold);
                        PostLabel.ForeColor = Color.Green;
                        PostLabel.Text = "Clear";
                        ListViewItem grey = new ListViewItem()
                        {
                            ForeColor = Color.DarkGray,
                            Text = line
                        };
                        listView1.Items.Add(grey);
                    }
                    else
                    {
                        PostLabel.Font = new Font("Arial", 18, FontStyle.Bold);
                        PostLabel.ForeColor = Color.Green;
                        PostLabel.Text = "Clear";
                        ListViewItem blue = new ListViewItem()
                        {
                            ForeColor = Color.Blue,
                            Text = line
                        };
                        listView1.Items.Add(blue);
                    }
                    string newstring = i.ToString();
                    string kosstring = newstring + " KoS In Local";
                    if (i >= 1)
                    {
                        PostLabel.Font = new Font("Arial", 18, FontStyle.Bold);
                        PostLabel.ForeColor = Color.Red;
                        PostLabel.Text = kosstring;
                        Clipboard.SetText(kosstring);
                    }
                }
            }
        }

        private void EveAPI(object sender, EventArgs e)
        {
            string keyid = "324382";
            string vcode = "fMrxN50fPetP6XJPgnCxZjZq9D3Z1Q9BNpW6lH01mjvURKn8TrvLIpOEGNseIBx0";
            string charid = "96761716";
            string url = "https://api.eveonline.com/char/ContactList.xml.aspx?keyID=" + keyid + "&vCode=" + vcode + "&characterID=" + charid;
            WebClient client = new WebClient();
            string s = client.DownloadString(url);
            XmlDocument eveapi = new XmlDocument();
            eveapi.LoadXml(s);
            eveapi.Save("EveAPI.xml");
         }

        private void CloseApp(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private bool cycle = true;
        private void CycleExpand(object sender, EventArgs e)
        {
            {
                if (cycle)
                {
                    miniModeToolStripMenuItem.Checked = true;
                    this.Size = new System.Drawing.Size(288, 59);
                }
                else
                {
                    miniModeToolStripMenuItem.Checked = false;
                    this.Size = new System.Drawing.Size(288, 250);
                }
                cycle = !cycle;
            }
        }

        private bool darkcycle = true;
        private void DarkModeCycle(object sender, EventArgs e)
        {
            {
                if (darkcycle)
                {
                    darkModeToolStripMenuItem.Checked = true;
                    BackColor = System.Drawing.Color.DimGray;
                    MiniCheck.BackColor = System.Drawing.Color.DimGray;
                    listView1.BackColor = System.Drawing.Color.DimGray;
                    this.ForeColor = System.Drawing.Color.White;
                    MiniCheck.ForeColor = System.Drawing.Color.White;
                    listView1.ForeColor = System.Drawing.Color.White;
                    ExpandButton.ForeColor = System.Drawing.Color.White;
                    ExpandButton.BackColor = System.Drawing.Color.DimGray;
                }
                else
                {
                    darkModeToolStripMenuItem.Checked = false;
                    this.BackColor = System.Drawing.Color.White;
                    MiniCheck.BackColor = System.Drawing.Color.White;
                    listView1.BackColor = System.Drawing.Color.White;
                    this.ForeColor = System.Drawing.Color.Black;
                    MiniCheck.ForeColor = System.Drawing.Color.Black;
                    listView1.ForeColor = System.Drawing.Color.Black;
                    ExpandButton.BackColor = System.Drawing.Color.White;
                    ExpandButton.ForeColor = System.Drawing.Color.Black;
                }
                darkcycle = !darkcycle;
            }
        }

        private bool staycycle = true;
        private void StayOnTopCycle(object sender, EventArgs e)
        {
            {
                if (staycycle)
                {
                    stayOnTopToolStripMenuItem.Checked = true;
                    this.TopMost = true;
                }
                else
                {
                    stayOnTopToolStripMenuItem.Checked = false;
                    this.TopMost = false;
                }
                staycycle = !staycycle;
            }
        }

        private void Opa100(object sender, EventArgs e)
        {
            toolStripMenuItem3.Checked = true;
            toolStripMenuItem4.Checked = false;
            toolStripMenuItem5.Checked = false;
            toolStripMenuItem6.Checked = false;
            this.Opacity = 1;
        }

        private void Opa75(object sender, EventArgs e)
        {
            toolStripMenuItem3.Checked = false;
            toolStripMenuItem4.Checked = true;
            toolStripMenuItem5.Checked = false;
            toolStripMenuItem6.Checked = false;
            this.Opacity = 0.75;
        }

        private void Opa50(object sender, EventArgs e)
        {
            toolStripMenuItem3.Checked = false;
            toolStripMenuItem4.Checked = false;
            toolStripMenuItem5.Checked = true;
            toolStripMenuItem6.Checked = false;
            this.Opacity = 0.5;
        }

        private void Opa25(object sender, EventArgs e)
        {
            toolStripMenuItem3.Checked = false;
            toolStripMenuItem4.Checked = false;
            toolStripMenuItem5.Checked = false;
            toolStripMenuItem6.Checked = true;
            this.Opacity = 0.25;
        }
    }
}

